let todoArray = [];

let counter = 0;
const buttonOnClick = liElementId => {
    const todoUlElement = document.querySelector("#todoUl");
    const todoLiElement = todoUlElement.querySelector("#" + liElementId);
    const todoLabelElement = todoUlElement.querySelector(".todoLabel");
    const todoText = todoLabelElement.innerHTML;
    todoUlElement.removeChild(todoLiElement);
    const foundTodo = todoArray.findIndex(todo => todo == todoText);
    if (foundTodo != -1)
        todoArray.splice(foundTodo, 1);
    sessionStorage.setItem(JSON.stringify(todoArray));
}

const renderTodo = (todoText) => {
    console.log({ todoText });

    const todoUlElement = document.querySelector("#todoUl");
    counter++;

    todoUlElement.insertAdjacentHTML('beforeend', `
    <li id = "todo-${counter}">
        <input class="todoCompleted" type="checkbox" />
        <label class= "todoLabel">${todoText}</label>
        <button class="todoDelete" onclick="buttonOnClick('todo-${counter}')"  >X</button>
    </li>
    `);

}

function addTodo() {

    const todoInputElement = document.querySelector("#textInput");
    renderTodo(todoInputElement.value);

    todoArray.push(todoInputElement.value);
    sessionStorage.setItem("mydata", JSON.stringify(todoArray));

    todoInputElement.value = " ";



}

function reloadTodo() {

    console.log("fetch HERE");

    const todoArray = JSON.parse(sessionStorage.getItem("mydata") || "[]");
    if (todoArray == null)
        todoArray = [];
    todoArray.forEach(todoText => {
        renderTodo(todoText);
    });
}
document.addEventListener("DOMContentLoaded", reloadTodo);



